# -*- coding: utf-8 -*-
"""
Created on Thu Jul 26 11:06:06 2018

@author: cwalker
"""

import pandas as pd
import glob
import lvm_read

all_data = {}
mean = {}
time = []
deltaT = []
filenames = glob.glob('C:/Users/cwalker/Desktop/myFiles/projects/desublimation/measurements/desub surfaces/AgI results/20170719/*.lvm') 


for filename in filenames:
    data_instance = lvm_read.read(filename)[0]['data']
    all_data[filename] = data_instance
    
for data_set in all_data:
     T = all_data[data_set][:,1]
     time.append(len(T)/10)
     deltaT.append(T[-1]-T[0])
#    data_instance = all_data[data_set]
#    mean[data_set] = data_instance.loc[:1].mean()